//
//  constants.swift
//  
//
//  Created by Gurpreet Gulati on 16/08/18.
//

import Foundation

enum APIConstants :String {
    case
    isSuccess = "isSuccess",
    data = "data",
    items = "items",
    pageNo = "pageNo",
    pageSize = "pageSize",
    total = "total",
    totalRecords = "totalRecords"
}

struct appConstants{
  static let token = "token"
  static let roomArraySave = "roomArraySave"
  static let password = "password"
  static let profile = "profile"
  static let orgToken = "orgToken"
  static let deviceId = "deviceId"
  static let id = "id"
  static let userId = "userId"
  static let alreadyLoggedIn = "alreadyLoggedIn"
  static let picUrls = "picUrls"
  static let isDiabetes = "isDiabetes"
  static let fcmToken = "fcmToken"
  static let email = "email"
  static let phone = "phone"
  static let code = "code"
  static let profileData = "profileData"
  static let designation = "designation"
  static let name = "name"
  static let censoredList = "censoredList"
  static let deviceType = "deviceType"
  static let username = "username"
  static let folder = "folder"
  static let task = "task"
  static let region = "region"
  static let country = "country"
  static let dob = "dob"
  static let zipCode = "zipCode"
  static let lga = "lga"
  static let town = "town"
  static let googleId = "googleId"
  static let facebookId = "facebookId"

  static let InAppPurchase = "InAppPurchase"
  static let propertyAddressLine1 = "propertyAddressLine1"
  static let ReportTypeName = "ReportTypeName"
  static let AboutUsVC = "AboutUsVC"
  static let PrivacyVC = "PrivacyVC"
  static let TermsConVC = "TermsConVC"
  static let rangeDistance = "rangeDistance"
  static let appleWatchName = "appleWatchName"

}

struct AppConstants{
    static let genderArray = ["Male","Female","Other","None"]
    static let rangeArray = ["0-5 ft","5-10 ft","10-15 ft","15-20 ft","20-25 ft"]
    static let rangeArrayMaxNo = [5.0,10.0,15.0,20.0,25.0]

}



struct Constants {
    static let kUserDefaults = UserDefaults.standard
}
let kAppDelegate = AppDelegate().sharedInstance()
