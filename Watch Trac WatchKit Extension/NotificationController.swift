//
//  NotificationController.swift
//  Watch Trac WatchKit Extension
//
//  Created by Sukhwinder Singh on 04/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import WatchKit
import Foundation
import UserNotifications


class NotificationController: WKUserNotificationInterfaceController,UNUserNotificationCenterDelegate {

    override init() {
        // Initialize variables here.
        super.init()
        
        // Configure interface objects here.
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    override func didReceive(_ notification: UNNotification) {
        // This method is called when a notification needs to be presented.
        // Implement it if you use a dynamic notification interface.
        // Populate your dynamic notification interface as quickly as possible.
    }
    override func didReceive(_ notification: UNNotification, withCompletion completionHandler: @escaping (WKUserNotificationInterfaceType) -> Swift.Void) {

      completionHandler(.default)
    }
    
    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
      // Check if the app is actually in the background
//      guard WKExtension.shared().applicationState != .background else {
//        completionHandler([.sound, .alert])
//        return
//      }
            
      // Figure out whether or not to display the notification based on the app mode and the
      // notification category
      let presentationOptions: UNNotificationPresentationOptions
//      if let rawMode = UserDefaults.standard.string(forKey: CurrentModeKey), Mode(rawValue: rawMode) == .secondary
//        || UserNotificationCategory(rawValue: notification.request.content.categoryIdentifier) == .repeating {
//        presentationOptions = [.sound, .alert]
//      } else {
//        presentationOptions = []
//      }

        completionHandler([.sound,.alert])
      
      // If the notification isn't presented, process it and clean it up
//      if presentationOptions.isEmpty {
//        // Request that the paired iPhone remove the notification from its notification center, if present
////        self.connectivityManager?.send(message: ClearNotificationCommand(identifier: notification.request.identifier))
//
//        // Process the notification, if desired
//      }
    }
}
