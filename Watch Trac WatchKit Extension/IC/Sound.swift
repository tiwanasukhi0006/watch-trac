//
//  Sound.swift
//  Watch Trac WatchKit Extension
//
//  Created by Sukhwinder Singh on 27/07/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import Foundation
import AVFoundation
class SoundFiles {
    
    var player = AVAudioPlayer()
    let audioSession = AVAudioSession.sharedInstance()

    func setupAvplayer(){
           do {
                  // Working Reroutes to headset
                  //            try session.setCategory(AVAudioSession.Category.playback,
                  //                                    mode: .default,
                  //                                    policy: .longForm,
                  //                                    options: [])
                  
                  // Plays in watch speaker
                  try audioSession.setCategory(AVAudioSession.Category.playback,
                                          mode: .default,
                                          policy: .default,
                                          options: [])
              } catch let error {
                  fatalError("*** Unable to set up the audio session: \(error.localizedDescription) ***")
              }
              if let path = Bundle.main.url(forResource: "emergency-alert", withExtension: "mp3") {
                  let fileUrl = path
                  do{
                      player = try AVAudioPlayer(contentsOf: fileUrl)
                  }
                  catch
                  {
                      print("*** Unable to set up the audio player: \(error.localizedDescription) ***")
                      // Handle the error here.
                      return
                  }
              }
       }
    
     func makeAlarmSound(){
        audioSession.activate(options: []) { (success, error) in
                guard error == nil else {
                          print("*** error occurred: \(error!.localizedDescription)***")
                          // Handle the error here.
                          return
                      }
                      if(success){
                          // Play the audio file.
                          self.player.play()
                      }
                  }
           }
    func stopSound(){
        self.player.stop()
    }
    
}
