//
//  WhereIsPhoneIC.swift
//  Watch Trac WatchKit Extension
//
//  Created by Sukhwinder Singh on 04/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import WatchKit
import Foundation


class WhereIsPhoneIC: WKInterfaceController {

    @IBOutlet weak var mWhereBtnOut: WKInterfaceButton!
    var soundF = SoundFiles()
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
//        mWhereBtnOut.cor
    }

    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        soundF.setupAvplayer()
        soundF.makeAlarmSound()
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }

    
    @IBAction func mGotItBtnAct() {
       DispatchQueue.main.async {
          self.dismiss()
        }
    }
    
    @IBAction func mMuteBtnAct() {
        soundF.stopSound()
    }
    
}
