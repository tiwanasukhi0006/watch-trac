//
//  HomeIC.swift
//  Watch Trac WatchKit Extension
//
//  Created by Sukhwinder Singh on 04/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity
import AVFoundation
import UserNotifications
class HomeIC: WKInterfaceController,WatchConnectivityManagerWatchDelegate,WatchConnectivityManagerPhoneDelegate {
   
    

    @IBOutlet weak var mDesLbl: WKInterfaceLabel!
    @IBOutlet weak var mOnLbl: WKInterfaceLabel!
    @IBOutlet weak var mOnButtonOut: WKInterfaceButton!
    
    var isOnBool : Bool = true
    var wcSession : WCSession!
    var player = AVAudioPlayer()
    let audioSession = AVAudioSession.sharedInstance()
    var blueTimer: Timer?

    ///////////////////////////////////////////////////////////////////////////////////
    
    override init() {
        super.init()
    }
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // The `WatchConnectivityManager` will provide tailored callbacks to its delegate.
        WatchConnectivityManager.sharedConnectivityManager.delegate = self
    }
    
    ///////////////////////////////////////////////////////////////////////////////////
    
    
    @IBAction func mOnBTnAct() {
        if isOnBool == false{
            setOffData()
          
        }else{
           setOnData()
        }
    }
    func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithDesignator designator: String, designatorColor: String) {
           
       }
    func stringWithUUID() -> String {
     let uuidObj = CFUUIDCreate(nil)
     let uuidString = CFUUIDCreateString(nil, uuidObj)!
     return uuidString as String
    }
    // Registering notification categories on Apple Watch
   
    func setOnData(){
        mOnButtonOut.setBackgroundImageNamed("offbutton")
        isOnBool = false
        mOnLbl.setText("Off")
        mDesLbl.setText("No Alarm")
        let message = ["message": "watchOn"]
        WCSession.default.transferUserInfo(message)
           WCSession.default.sendMessage(message, replyHandler: nil) { (error) in
                                  print(error.localizedDescription)
                }
//        self.wcSession.sendMessage(message, replyHandler: nil) { (error) in
//            print(error.localizedDescription)
//        }

    }

    func setOffData(){
        mOnButtonOut.setBackgroundImageNamed("onButton")
        isOnBool = true
        mOnLbl.setText("On")
        mDesLbl.setText("If watch seperates from phone, sound alarm.")
        let message = ["message": "watchOff"]
        WCSession.default.transferUserInfo(message)
        WCSession.default.sendMessage(message, replyHandler: nil) { (error) in
                          print(error.localizedDescription)
        }

    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()

    }
   
    func repeatScanning(){
       DispatchQueue.main.async {
          let name =  WKInterfaceDevice.current().name
            print(name)
            let message = ["message": name]
             WCSession.default.transferUserInfo(message)
             }
       }
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    // MARK: WatchConnectivityManagerWatchDelegate
       
       func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithMorseCode morseCode: String) {
           print(morseCode)
//        print("message received")

        if morseCode == "showAlertVC"{
      //  DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
        //    DispatchQueue.main.async {
                self.setNo()
          //  }
       //  }
//          self.mOnLbl.setText(morseCode)
          self.mDesLbl.setText("If watch seperates from phone, sound alarm.")
          self.presentController(withName: "WhereIsPhoneICID", context: nil)
        }else if morseCode == "watchOn"{
          self.setOnData()
         }else if morseCode == "watchOff"{
          self.setOffData()
         }else if morseCode == "watchName"{
            self.repeatScanning()
        }
       }
    
      func setNo(){
        let content = UNMutableNotificationContent()
        content.title = "No Device Left behind"
//        content.subtitle = "Do you know?"
        content.body = "Where is your phone?"
        content.badge = 1
        let alarmId = UUID().uuidString

        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 1, repeats: false)
        let request = UNNotificationRequest(identifier: "\(alarmId)", content: content, trigger: trigger)
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        print("*********fire********")
    }

}

extension HomeIC : WCSessionDelegate{
////
//    // MARK: WCSession Methods
     func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        print("message received")
//        mOnLbl.setText("message received")
//
//        let text = message["message"] as! String
//        if text == "showAlert"{
//            let message = ["message": "showAlertVC"]
//            self.wcSession.sendMessage(message, replyHandler: nil) { (error) in
//              print(error.localizedDescription)
//            }
//           self.mDesLbl.setText("If watch seperates from phone, sound alarm.")
//           presentController(withName: "WhereIsPhoneICID", context: nil)
//
//        }else if text == "watchOn"{
//            setOnData()
//        }else if text == "watchOff"{
//            setOffData()
//
//        }

     }

     func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
         print("activationState: \(activationState)")
         // Code.
 //       mOnLbl.setText("activationState")
     }
   func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
    print("message didReceiveApplicationContext done .............................")
    DispatchQueue.main.async{

    }
  }
//
}
