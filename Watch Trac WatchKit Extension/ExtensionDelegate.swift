//
//  ExtensionDelegate.swift
//  Watch Trac WatchKit Extension
//
//  Created by Sukhwinder Singh on 04/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import WatchKit
import UserNotifications
import WatchConnectivity

class ExtensionDelegate: NSObject, WKExtensionDelegate,UNUserNotificationCenterDelegate {
    
    var wcBackgroundTasks: [WKWatchConnectivityRefreshBackgroundTask]

    let center = UNUserNotificationCenter.current()

    override init() {
         wcBackgroundTasks = []
         super.init()
         
         let defaultSession = WCSession.default
         defaultSession.delegate = WatchConnectivityManager.sharedConnectivityManager
         
         /*
          Here we add KVO on the session properties that this class is interested in before activating
          the session to ensure that we do not miss any value change events
          */
         defaultSession.addObserver(self, forKeyPath: "activationState", options: [], context: nil)
         defaultSession.addObserver(self, forKeyPath: "hasContentPending", options: [], context: nil)
         
         defaultSession.activate()
          center.delegate = self
          let options: UNAuthorizationOptions = [.alert, .badge, .sound]
           center.requestAuthorization(options: options) { (granted, error) in
             if granted {
                WKExtension.shared().registerForRemoteNotifications()
                }
            }
     }
     
     override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
         DispatchQueue.main.async {
             self.completeAllTasksIfReady()
         }
     }
    // MARK: Convenience
      
      func completeAllTasksIfReady() {
          let session = WCSession.default
          // the session's properties only have valid values if the session is activated, so check that first
          if session.activationState == .activated && !session.hasContentPending {
              wcBackgroundTasks.forEach { $0.setTaskCompleted() }
              wcBackgroundTasks.removeAll()
          }
      }
    
//
//    func applicationDidFinishLaunching() {
//        // Perform any final initialization of your application.
//        if WCSession.isSupported() {
//            let session = WCSession.default
//            session.delegate = self
//            session.activate()
//        }
//
//        if WCSession.default.isReachable {
//            let messageDict = ["message": "hello iPhone!"]
//            WCSession.default.sendMessage(messageDict, replyHandler: { (replyDict) -> Void in
//                print(replyDict)
//                }, errorHandler: { (error) -> Void in
//                print(error)
//            })
//        }
//
//         center.delegate = self
//         let options: UNAuthorizationOptions = [.alert, .badge, .sound]
//          center.requestAuthorization(options: options) { (granted, error) in
//             if granted {
//                 WKExtension.shared().registerForRemoteNotifications()
//                 }
//              }
//        setUp()
//    }
   
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("i received a notification")
        completionHandler()
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .sound,.badge])
    }
    
   
  
    
    func applicationDidBecomeActive() {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
       
    }

    func applicationWillResignActive() {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, etc.
    }
    
    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
           for backgroundTask in backgroundTasks {
               if let wcBackgroundTask = backgroundTask as? WKWatchConnectivityRefreshBackgroundTask {
                   // store a reference to the task objects as we might have to wait to complete them
                   self.wcBackgroundTasks.append(wcBackgroundTask)
               } else {
                   // immediately complete all other task types as we have not added support for them
                   backgroundTask.setTaskCompleted()
               }
           }
           completeAllTasksIfReady()
       }
    

//    func handle(_ backgroundTasks: Set<WKRefreshBackgroundTask>) {
//        // Sent when the system needs to launch the application in the background to process tasks. Tasks arrive in a set, so loop through and process each one.
//
//        for task in backgroundTasks {
//
//            // Use Logger to log the tasks for debug purpose. A real app may remove the log
//            // to save the precious background time.
//            //
//            switch task {
//            case let backgroundTask as WKApplicationRefreshBackgroundTask:
//                // Be sure to complete the background task once you’re done.
//                backgroundTask.setTaskCompletedWithSnapshot(false)
//            case let snapshotTask as WKSnapshotRefreshBackgroundTask:
//                // Snapshot tasks have a unique completion call, make sure to set your expiration date
//                snapshotTask.setTaskCompleted(restoredDefaultState: true, estimatedSnapshotExpiration: Date.distantFuture, userInfo: nil)
//            case let connectivityTask as WKWatchConnectivityRefreshBackgroundTask:
//                // Be sure to complete the connectivity task once you’re done.
//                connectivityTask.setTaskCompletedWithSnapshot(false)
//            case let urlSessionTask as WKURLSessionRefreshBackgroundTask:
//                // Be sure to complete the URL session task once you’re done.
//                urlSessionTask.setTaskCompletedWithSnapshot(false)
//            case let relevantShortcutTask as WKRelevantShortcutRefreshBackgroundTask:
//                // Be sure to complete the relevant-shortcut task once you're done.
//                relevantShortcutTask.setTaskCompletedWithSnapshot(false)
//            case let intentDidRunTask as WKIntentDidRunRefreshBackgroundTask:
//                // Be sure to complete the intent-did-run task once you're done.
//                intentDidRunTask.setTaskCompletedWithSnapshot(false)
//            default:
//                // make sure to complete unhandled task types
//                task.setTaskCompletedWithSnapshot(false)
//            }
//        }
//
//
//    }
}
//extension ExtensionDelegate : WCSessionDelegate{
//
//    // MARK: WCSession Methods
//     func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
//        print("message received")
//
//
//     }
//
//     func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
//         print("activationState: \(activationState)")
//         // Code.
// //       mOnLbl.setText("activationState")
//     }
//   func session(_ session: WCSession, didReceiveApplicationContext applicationContext: [String : Any]) {
//    print("message didReceiveApplicationContext done .............................")
//    DispatchQueue.main.async{
//
//    }
//  }
//
//    func sessionReachabilityDidChange(_ session: WCSession) {
//
//    }
//
//}
