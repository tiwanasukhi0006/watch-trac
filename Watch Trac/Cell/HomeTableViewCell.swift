//
//  HomeTableViewCell.swift
//  Watch App
//
//  Created by Sukhwinder Singh on 01/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var mMakeSureLbl: UILabel!
    @IBOutlet weak var mActivityLoader: UIActivityIndicatorView!
    @IBOutlet weak var mWatchImg: UIImageView!
    @IBOutlet weak var mTitleNameLbl: UILabel!
    @IBOutlet weak var mAwayLbl: UILabel!
    @IBOutlet weak var mSwitchBtn: UISwitch!
    
    @IBOutlet weak var mNoDataLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
