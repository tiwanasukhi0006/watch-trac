//
//  SideMneuTableViewCell.swift
//  Watch App
//
//  Created by Sukhwinder Singh on 01/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import UIKit

class SideMneuTableViewCell: UITableViewCell {

    @IBOutlet weak var mImage: UIImageView!
    @IBOutlet weak var mNameLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
