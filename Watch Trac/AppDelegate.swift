//
//  AppDelegate.swift
//  Watch Trac
//
//  Created by Sukhwinder Singh on 04/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

//com.watchapp.bcoder.Watch-Trac-new
//com.watchapp.bcoder.Watch-Trac.watchkitapp-new
//com.watchapp.bcoder.Watch-Trac.watchkitapp.watchkitextension-new

//com.watchapp.intutive.Watch-Trac
//com.watchapp.intutive.Watch-Trac.watchkitapp
//com.watchapp.intutive.Watch-Trac.watchkitapp.watchkitextension
import UIKit
import WatchConnectivity
import UserNotifications
import IQKeyboardManagerSwift
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

let kConstantObj = kConstant()


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,WCSessionDelegate,UNUserNotificationCenterDelegate,MessagingDelegate {

    var window: UIWindow?
    static var originalAppDelegate : AppDelegate!
       func sharedInstance() -> AppDelegate{
           return AppDelegate.originalAppDelegate
       }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.enable = true
        AppDelegate.originalAppDelegate = self
        FirebaseApp.configure()
        if #available(iOS 13, *){
            // iOS 11 (or newer) Swift code
          }else{
            // iOS 10 or older code
            initialView()
          }
        if WCSession.isSupported() {
            let defaultSession = WCSession.default
            defaultSession.delegate = WatchConnectivityManager.sharedConnectivityManager
            defaultSession.activate()
            }

        // Override point for customization after application launch.
//         if WCSession.isSupported() { // check if the device support to handle an Apple Watch
//             let session = WCSession.default
//                 session.delegate = self
//                  session.activate() // activate the session
//
//                if session.isPaired { // Check if the iPhone is paired with the Apple Watch
//                                 // Do stuff
//                    print("watch is paired")
//                      }else{
//                        print("watch is not paired")
//                      }
//                     }
         setDeviceForPushNotification(application)

        return true
    }
    func initialView(){
           let mainVcIntial = kConstantObj.SetIntialMainViewController("HomeVCID")
                 self.window?.rootViewController = mainVcIntial
       }
    func setDeviceForPushNotification(_ application : UIApplication){
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        }else{
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        application.registerForRemoteNotifications()
    }
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
          print("Firebase registration token: \(fcmToken)")
          let token = Messaging.messaging().fcmToken
          print("FCM token: \(token ?? "")")
          Constants.kUserDefaults.set(token, forKey: appConstants.fcmToken)
      }

      func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
          
          print("Received data message: \(remoteMessage.appData)")
          
          guard let data: [String: Any] = remoteMessage.appData as? [String: Any] else {
              return
          }
          
          print(data)
          
        
          
      }
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
          print("Received data message: \(remoteMessage.appData)")
      }
      func application(_ application: UIApplication,
                       didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
          
          InstanceID.instanceID().instanceID { (result, error) in
              if let error = error {
                  print("Error fetching remote instange ID: \(error)")
              } else if let result = result {
                  print("Remote instance ID token: \(result.token)")
                  Constants.kUserDefaults.set(result.token, forKey: appConstants.fcmToken)
              }
          }
          Messaging.messaging().apnsToken = deviceToken as Data
          
          
      }
      func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
          print(error)
          UserDefaults.standard.setValue("", forKey: "DeviceToken")
      }
      
      func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
          print(userInfo)
       
      }
     
//    // MARK: UISceneSession Lifecycle
      func setNotification (){
            UNUserNotificationCenter.current().delegate = self
            let content = UNMutableNotificationContent()
            content.title = NSString.localizedUserNotificationString(forKey: "No device left behind", arguments: nil)
                    
            content.userInfo = ["type":
                "","taskId":"78"]
            content.body = NSString.localizedUserNotificationString(forKey: "Watch Trac",arguments: nil)
                      
            content.sound = UNNotificationSound.default
            UNUserNotificationCenter.current().getNotificationSettings(){ (settings) in
                 
            }
            var dateComponents = DateComponents()
            let calendar = Calendar.current
            let year =  calendar.component(.year, from: Date())
            let month = calendar.component(.month, from: Date())
            let day = calendar.component(.day, from: Date())

                dateComponents.hour = 15
                dateComponents.minute = 34
                dateComponents.month = month
                dateComponents.year = year
                dateComponents.day = day
                      let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)

                      content.categoryIdentifier = "todoList"

                      let request = UNNotificationRequest(identifier: String(describing: "23"), content: content, trigger: trigger)
                      UNUserNotificationCenter.current().delegate = self
              //        UNUserNotificationCenter.current().setNotificationCategories([category])

                      UNUserNotificationCenter.current().add(request) {(error) in
                          if let error = error {
                              print("Uh oh! We had an error: \(error)")
                          }
                      }
                  }

    
    
    
       func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
                completionHandler([.alert,.badge, .sound])
            }
            func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
                print(response.notification.request.content.userInfo)
                print(response)
                var type = String()
          
                type = response.notification.request.content.userInfo["type"] as! String
                if response.notification.request.content.userInfo["taskId"] != nil{
                }
    //           NotificationCenter.default.post(name: Notification.Name(appConstants.dailyVC), object: nil)
                if UIApplication.shared.applicationState == .active{
                    print(response.notification.request.content)
                    if response.actionIdentifier == "didit"{
//                        self.pushViewController(type: type)
                    }else if response.actionIdentifier == "later"{ print("Not detect")
                    }else{
//                        self.pushViewController(type: type)
                    }            }else if UIApplication.shared.applicationState == .inactive{
                    if response.actionIdentifier == "didit"{
//                        self.pushViewController(type: type)
                    }else if response.actionIdentifier == "later"{ print("Not detect")
                    }else{
//                        self.pushViewController(type: type)
                    }
                }else if UIApplication.shared.applicationState == .background{
                    if response.actionIdentifier == "didit"{
                    }else{ print("Not detect")
                    }
                }else{
                    print("background state")
                    if response.actionIdentifier == "didit"{
//                        self.pushViewController(type: type)
                    }else if response.actionIdentifier == "later"{ print("Not detect")
                    }else{
//                        self.pushViewController(type: type)
                    }
                }
                completionHandler()
            }
       
      
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    private func session(session: WCSession, didReceiveMessage message: [String : AnyObject], replyHandler: ([String : AnyObject]) -> Void) {
        replyHandler(["message": "Hello Watch!" as AnyObject])
    }
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
   
    func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithMorseCode morseCode: String) {
        print("*******************************************************************************************GET NOT IN IPHONE************************************************************")
   
       
       }
}

