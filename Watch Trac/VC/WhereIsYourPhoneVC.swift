//
//  WhereIsYourPhoneVC.swift
//  Watch Trac
//
//  Created by Sukhwinder Singh on 27/07/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import UIKit

class WhereIsYourPhoneVC: UIViewController {

    @IBOutlet weak var mGotItOut: UIButton!
    
    var soundFVC = SoundFilesForApp()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mGotItOut.layer.cornerRadius = 25
        mGotItOut.layer.masksToBounds = true
        soundFVC.playSound()
    }
    

    @IBAction func mGotItBtnAct(_ sender: Any) {
        soundFVC.stopSound()
        dismiss(animated: true, completion: nil)
    }
    
    
}
