//
//  HomeVC.swift
//  Watch App
//
//  Created by Sukhwinder Singh on 01/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import UIKit
import WatchConnectivity
import CoreBluetooth
import UserNotifications

class HomeVC: UIViewController,WatchConnectivityManagerPhoneDelegate,WatchConnectivityManagerWatchDelegate {

    @IBOutlet weak var mPairTableVw: UITableView!
    @IBOutlet weak var mContainVw: UIView!
    @IBOutlet weak var mCstntScrollVwHgt: NSLayoutConstraint!
    @IBOutlet weak var mOnView: UIView!
    
    @IBOutlet weak var mOffView: UIView!
    @IBOutlet weak var mScrollVw: UIScrollView!
    
    
    
    var isWatchPaired : Bool = false
    var manager:CBCentralManager!

//    var peripherals:[CBPeripheral] = []
    var peripheralRSSIs = Dictionary<CBPeripheral,NSNumber>()
    var peripherals = Set<CBPeripheral>()
    var rssiArray = [Double]()
    
    var parentView:HomeVC? = nil

    var wcSession : WCSession! = nil
    var watchName = String()
    var refreshControl = UIRefreshControl()
    var blueTimer: Timer?
    var distanceInFeet = Double()
    var rssiSortedValue = Double()
    var isAlertActivate : Bool = true
    var blueTimerForName: Timer?
    var getWatchName : Bool = false
    ///////////////////////////////////////////////////////////////////////////////////
    
    convenience init() {
        self.init(nibName:nil, bundle:nil)
        WatchConnectivityManager.sharedConnectivityManager.delegate = self

    }
//    override class func awakeFromNib() {
//        super.awakeFromNib()
//        WatchConnectivityManager.sharedConnectivityManager.delegate = self
//
//    }
    ///////////////////////////////////////////////////////////////////////////////////
    override func viewDidLoad() {
        super.viewDidLoad()
        mOnView.isHidden = false
        mOffView.isHidden = true
        mPairTableVw.isHidden = false
        if let watchnm = Constants.kUserDefaults.value(forKey: appConstants.appleWatchName)as? String{
            watchName = watchnm
        }
      //  checkWatchIsConneted()
        setupBluetooth()
        scanBLEDevice()
        addRefreshController()
        addTimerForScanningPurpose()
        checkIfWatchNameNotHere()
    }
    
    func checkIfWatchNameNotHere(){
        blueTimerForName = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(repeatScanningName), userInfo: nil, repeats: true)

    }
    @objc func repeatScanningName(){
       DispatchQueue.main.async {
        if self.getWatchName == false{
            let message = ["message": "watchName"]
            WCSession.default.transferUserInfo(message)
        }else{
            self.blueTimerForName?.invalidate()

        }
//        if self.watchName == ""{
//            let message = ["message": "watchName"]
//            WCSession.default.transferUserInfo(message)
//
//        }else{
//            self.blueTimerForName?.invalidate()
//        }
        
        }
    }
    
    // MARK: WatchConnectivityManagerDelegate
      
      func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithDesignator designator: String, designatorColor: String) {
        print(designator)
        print(designatorColor)
          DispatchQueue.main.async(execute: {
          
            if designator == "watchOn"{
              self.mOnView.isHidden = true
              self.mOffView.isHidden = false
              self.mPairTableVw.isHidden = true
              self.stopScanForBLEDevice()
              
            }else if designator == "watchOff"{
                self.mOnView.isHidden = false
                self.mOffView.isHidden = true
                self.mPairTableVw.isHidden = false
                self.scanBLEDevice()
                self.manager?.scanForPeripherals(withServices: nil, options: nil)
              
            }else if designator == "showAlertVC"{
                               // self.addAlertView()
              
            }else if designator == "watchName"{
              self.watchName = designator
              self.isWatchPaired = true
              self.getWatchName = true
              Constants.kUserDefaults.set(designator, forKey: appConstants.appleWatchName)
              self.mPairTableVw.reloadData()
            }else{
              self.watchName = designator
              self.isWatchPaired = true
              self.getWatchName = true
              Constants.kUserDefaults.set(designator, forKey: appConstants.appleWatchName)
              self.mPairTableVw.reloadData()
                }
            
          })
      }
  
    func addTimerForScanningPurpose(){
        blueTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(repeatScanning), userInfo: nil, repeats: true)
    }
    
    @objc func repeatScanning(){
        manager?.scanForPeripherals(withServices: nil, options: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        WatchConnectivityManager.sharedConnectivityManager.delegate = self
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.isHidden = false
        setHeightOfScrollView()
    }
    
    func addRefreshController(){
        self.refreshControl.tintColor = UIColor.white
        self.refreshControl.attributedTitle = NSAttributedString(string: "")
        mScrollVw.alwaysBounceVertical = true
        mScrollVw.bounces  = true
        refreshControl.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        self.mScrollVw.addSubview(refreshControl)
    }
    
    @objc func didPullToRefresh() {
        print("Refersh")
        manager?.scanForPeripherals(withServices: nil, options: nil)
       // checkWatchIsConneted()
        refreshControl.endRefreshing()


    }

    func addAlertView(){
        let resultController = self.storyboard?.instantiateViewController(withIdentifier: "WhereIsYourPhoneVCID") as! WhereIsYourPhoneVC
         self.navigationController?.definesPresentationContext = true
         resultController.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
          resultController.modalTransitionStyle = .crossDissolve
          self.present(resultController, animated: true, completion: nil)
    }
    
    func setHeightOfScrollView(){
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0) + 50
        let viewHgt = view.frame.size.height - topBarHeight
        self.mCstntScrollVwHgt.constant = viewHgt
        self.mContainVw.layoutIfNeeded()
    }
    
    
    @IBAction func mSwitchOnBtnAct(_ sender: Any) {
        setOnData()

    }
    
    func setOnData(){
        mOnView.isHidden = true
        mOffView.isHidden = false
        mPairTableVw.isHidden = true
        stopScanForBLEDevice()
        let message = ["message": "watchOn"]
        WCSession.default.transferUserInfo(message)
        
        blueTimer?.invalidate()
    }

    func setOffData(){
        mOnView.isHidden = false
        mOffView.isHidden = true
        mPairTableVw.isHidden = false
        scanBLEDevice()

        let message = ["message": "watchOff"]
        WCSession.default.transferUserInfo(message)

    }
    
    
    @IBAction func mSideMneuBtnAct(_ sender: Any) {
        sideMenuVC.toggleMenu()
    }
    
    @IBAction func mSwitchOffBtnAct(_ sender: Any) {
        setOffData()
    }
    
}
extension HomeVC : UITableViewDelegate,UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: view.frame.origin.x,y: 0 , width: view.frame.size.width ,height: 60))
        headerView.backgroundColor = .clear
        let labelText = UILabel(frame: CGRect(x: 17,y: 0 , width: headerView.frame.size.width ,height: 60))
        labelText.text = "Paired Device"
        labelText.font = UIFont.boldSystemFont(ofSize: 28.0)
        labelText.textColor = .white
        
        headerView.addSubview(labelText)
        return headerView
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if rssiArray.count > 0{
            isWatchPaired = true
          return 1
        }
        isWatchPaired = false
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTableViewCellID")as! HomeTableViewCell
        if isWatchPaired == true{
            cell.mWatchImg.isHidden = false
            cell.mTitleNameLbl.isHidden = false
            cell.mAwayLbl.isHidden = false
            cell.mSwitchBtn.isHidden = false
            cell.mNoDataLbl.isHidden = true
            cell.mActivityLoader.isHidden = true
            cell.mMakeSureLbl.isHidden = true
            cell.mTitleNameLbl.text = watchName
            let calculate = pow(10, ((-69 - rssiSortedValue)/20))
            let cal = calculate * 3.281
            let doubleStr = String(format: "%.2f", cal) // "3.14"
            cell.mAwayLbl.text = "\(doubleStr) ft Away"
            self.checkRssiValueForAlert(distance: cal.rounded())

        }else{
            cell.mWatchImg.isHidden = true
            cell.mTitleNameLbl.isHidden = true
            cell.mAwayLbl.isHidden = true
            cell.mSwitchBtn.isHidden = true
            cell.mNoDataLbl.isHidden = false
            cell.mNoDataLbl.text = "Scanning for devices..."
            cell.mActivityLoader.isHidden = false
            cell.mMakeSureLbl.isHidden = false
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 93
    }
    
 
}
extension UIViewController {
    
    /**
     *  Height of status bar + navigation bar (if navigation bar exist)
     */
    
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
}
extension HomeVC : WCSessionDelegate{
    func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        
    }
    
    func sessionDidBecomeInactive(_ session: WCSession) {
        
    }
    
    func sessionDidDeactivate(_ session: WCSession) {
        
    }
    
    func checkWatchIsConneted(){
      wcSession = WCSession.default
      wcSession.delegate = self
      wcSession.activate()
        isWatchPaired = false 
        if WCSession.isSupported() {
            let wcsession = WCSession.default
                wcsession.delegate = self
                wcsession.activate()
             if wcsession.isPaired { // Check if the iPhone is paired with the Apple Watch
                                     // Do stuff
                            isWatchPaired = true
                          }else{
                           // print("watch is not paired")
                            isWatchPaired = false
                }
           }
        
      }
    
    
    func watchConnectivityManager(_ watchConnectivityManager: WatchConnectivityManager, updatedWithMorseCode morseCode: String) {
//         DispatchQueue.main.async {
//            if morseCode == "hello iPhone!"{
//                  let alert = UIAlertController(title: "No device left", message: "hello iPhone!", preferredStyle: UIAlertController.Style.alert)
//                  alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
//                  self.present(alert, animated: true, completion: nil)
//              }
//              if morseCode == "watchOn"{
//                  self.mOnView.isHidden = true
//                  self.mOffView.isHidden = false
//                  self.mPairTableVw.isHidden = true
//                  self.stopScanForBLEDevice()
//
//              }else if morseCode == "watchOff"{
//                  self.mOnView.isHidden = false
//                  self.mOffView.isHidden = true
//                  self.mPairTableVw.isHidden = false
//                  self.scanBLEDevice()
//                  self.manager?.scanForPeripherals(withServices: nil, options: nil)
//
//              }else if morseCode == "showAlertVC"{
//                 // self.addAlertView()
//
//              }else{
//              self.watchName = morseCode
//              self.isWatchPaired = true
//              self.mPairTableVw.reloadData()
//              }
//
//
//          }
    
    }
    func session(_ session: WCSession, didReceiveMessage message: [String : Any]) {
        DispatchQueue.main.async {

          if let text = message["message"] as? String{
            if text == "hello iPhone!"{
                let alert = UIAlertController(title: "No device left", message: "hello iPhone!", preferredStyle: UIAlertController.Style.alert)
                alert.addAction(UIAlertAction(title: "Click", style: UIAlertAction.Style.default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
            if text == "watchOn"{
                self.mOnView.isHidden = true
                self.mOffView.isHidden = false
                self.mPairTableVw.isHidden = true
                self.stopScanForBLEDevice()

            }else if text == "watchOff"{
                self.mOnView.isHidden = false
                self.mOffView.isHidden = true
                self.mPairTableVw.isHidden = false
                self.scanBLEDevice()
                self.manager?.scanForPeripherals(withServices: nil, options: nil)

            }else if text == "showAlertVC"{
               // self.addAlertView()

            }else{
               self.watchName = text
               self.isWatchPaired = true
               self.mPairTableVw.reloadData()
            }
          }

        }
    }
    
}
//MARK:- Need Bluetooh setup
extension HomeVC:CBCentralManagerDelegate,CBPeripheralDelegate{
    func setupBluetooth(){
           manager = CBCentralManager()
           manager.delegate = self
       }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch central.state {
          case .poweredOn:
            manager?.scanForPeripherals(withServices: nil, options: nil)
              break
          case .poweredOff:
              print("Bluetooth is Off.")
              break
          case .resetting:
              break
          case .unauthorized:
              break
          case .unsupported:
              break
          case .unknown:
              break
          default:
              break
          }
    }
    
   func scanBLEDevice(){
         manager = CBCentralManager(delegate: self, queue: nil)
    }
    
    func stopScanForBLEDevice(){
        manager?.stopScan()
        print("scan stopped")
    }

    func centralManager(central: CBCentralManager, didConnectPeripheral peripheral: CBPeripheral) {
           print("connected!")
            peripheral.delegate=self
            peripheral.readRSSI()
       }

      
    //CBCentralMaganerDelegate code
       func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        peripheral.delegate=self
        peripheral.readRSSI()
        if watchName == peripheral.name {
            self.peripherals.insert(peripheral)
            self.peripheralRSSIs[peripheral]=RSSI
            central.connect(peripheral,options:nil)
          if RSSI != 127 {
                print("RSSI: \(RSSI)")
                if self.rssiArray.count < 3{
                   self.rssiArray.append(Double(truncating: RSSI))
                   rssiSortedValue = calculateRSSIAverage(rssiArr: self.rssiArray)
                }else{
                   self.rssiArray.remove(at: 0)
                   self.rssiArray.append(Double(truncating: RSSI))
                   rssiSortedValue = calculateRSSIAverage(rssiArr: self.rssiArray)
                }
            self.mPairTableVw.reloadData()
          }
        }
      }
    
    func calculateRSSIAverage(rssiArr:[Double])->Double{
        let total = rssiArr.reduce(0, +)
        let avg = total / Double(rssiArr.count)
        print("ARRAY: \(rssiArr)   COUNT: \(rssiArr.count)")
        print("AVERAGE: \(avg)")
        return avg
    }
    
    func checkRssiValueForAlert(distance:Double){
        var valueDis = Double()
        if let range = Constants.kUserDefaults.value(forKey: appConstants.rangeDistance)as? Double{
            valueDis = range
        }else{
            valueDis = 15.0
            Constants.kUserDefaults.set(valueDis, forKey: appConstants.rangeDistance)
        }
        if distance >  valueDis{
            print("go near")
            if isAlertActivate == true{
            let message = ["message":"showAlertVC"]
           // wcSession.sendMessage(message, replyHandler: nil) { (error) in
            //print(error.localizedDescription)
            //}
            WCSession.default.transferUserInfo(message)
            self.addAlertView()
            isAlertActivate = false
            }
        }else{
            isAlertActivate = true
            
        }
        
    }
    func calculateNewDistance(txCalibratedPower: Int, rssi: Int) -> Double{
        if rssi == 0{
            return -1
        }
        let ratio = Double(exactly:rssi)!/Double(txCalibratedPower)
        if ratio < 1.0{ return pow(10.0, ratio) }else{
            let accuracy = 0.89976 * pow(ratio, 7.7095) + 0.111
            return accuracy
        }
        
    }


    func peripheral(_ peripheral: CBPeripheral, didReadRSSI RSSI: NSNumber, error: Error?) {
       if (error != nil) {
        //    print(error)
        } else {
        if watchName == peripheral.name{
          self.peripheralRSSIs[peripheral]=RSSI
          self.mPairTableVw.reloadData()
          }
        }
    }
       func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {

       }

       func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
           //print(error!)
       }
    
    
    
}
