//
//  SideMenuVC.swift
//  Watch App
//
//  Created by Sukhwinder Singh on 01/05/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import UIKit
import MessageUI

class SideMenuVC:UIViewController,MFMailComposeViewControllerDelegate {
    let sideMenuVC = KSideMenuVC()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)



   let imageArray = ["share","report","distance"]
    let namesArray = ["Share App","Report a Bug","Select Range"]

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
               let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
               let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!

               let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
               let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
               let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
               let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
               let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")

               if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
                   return gmailUrl
               } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
                   return outlookUrl
               } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
                   return yahooMail
               } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
                   return sparkUrl
               }

               return defaultUrl
           }

           func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
               controller.dismiss(animated: true)
           }
    
}
extension SideMenuVC : UITableViewDelegate,UITableViewDataSource{
   
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return namesArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMneuTableViewCellID")as! SideMneuTableViewCell
        cell.mNameLbl.text = namesArray[indexPath.row]
        cell.mImage.image = UIImage(named: imageArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
         
            case 0:
            let activityVC = UIActivityViewController(activityItems: ["Checkout this app and never lose your phone again: www.nodeviceleftbehind.com"], applicationActivities: nil)
                activityVC.popoverPresentationController?.sourceView = self.view
                present(activityVC, animated: true, completion: nil)
                activityVC.completionWithItemsHandler = { (activityType, completed:Bool, returnedItems:[Any]?, error: Error?) in

                    if completed  {
                        self.dismiss(animated: true, completion: nil)
                    }
                }
             break
                
            case 1:
                  let recipientEmail = "nodeviceleftbehind@gmail.com"
                             let subject = ""
                             let body = ""
                             // Show default mail composer
                             if MFMailComposeViewController.canSendMail() {
                                 let mail = MFMailComposeViewController()
                                 mail.mailComposeDelegate = self
                                 mail.setToRecipients([recipientEmail])
                                 mail.setSubject(subject)
                                 mail.setMessageBody(body, isHTML: false)

                                 present(mail, animated: true)

                             // Show third party email composer if default Mail app is not present
                             } else if let emailUrl = createEmailUrl(to: recipientEmail, subject: subject, body: body) {
                                 UIApplication.shared.open(emailUrl)
                             }
                    
             break
                    
            case 2:
                  kConstantObj.SetIntialMainViewController("RangeDistanceVCID")

            break
            
            case 3:
//              let vc = storyboard?.instantiateViewController(withIdentifier: "TermsVCID") as! TermsVC
//               vc.whichVC = appConstants.TermsConVC
          //    self.navigationController?.pushViewController(vc, animated: true)

            break
                    
            default:
                    print("abc")
                }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}
