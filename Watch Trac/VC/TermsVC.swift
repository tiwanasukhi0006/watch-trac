//
//  TermsVC.swift
//  Watch Trac
//
//  Created by Sukhwinder Singh on 06/07/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import UIKit
import WebKit

class TermsVC: UIViewController,WKUIDelegate {

    var whichVC = String()
    var webView: WKWebView!

    override func loadView() {
      let webConfiguration = WKWebViewConfiguration()
       webView = WKWebView(frame: .zero, configuration: webConfiguration)
       webView.uiDelegate = self
       view = webView
      //  view.addSubview(webView)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

     if appConstants.TermsConVC == whichVC{
         let url = URL (string: "https://nodeviceleftbehind.com/terms-of-service/")
         let requestObj = URLRequest(url: url!)
         webView.load(requestObj)
        // self.navigationItem.title = "TERMS & CONDITIONS"

   }else{
        let url = URL (string: "https://nodeviceleftbehind.com/privacy-policy/")
        let requestObj = URLRequest(url: url!)
        webView.load(requestObj)
        //self.navigationItem.title = "TERMS & CONDITIONS"
   }
    }
    override func viewWillAppear(_ animated: Bool) {
          super.viewWillAppear(animated)
          self.navigationController?.navigationBar.isHidden = false
          self.setNeedsStatusBarAppearanceUpdate()
          self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
          self.navigationController?.navigationBar.shadowImage = UIImage()
          self.navigationController?.navigationBar.isTranslucent = true
      }
      
      override func viewWillDisappear(_ animated: Bool) {
          super.viewWillDisappear(animated)
          self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
          self.navigationController?.navigationBar.shadowImage = nil
          self.navigationController?.navigationBar.isTranslucent = true
      }
    @IBAction func mBackBtnAct(_ sender: Any) {
//        navigationController?.popViewController(animated: true)
        kConstantObj.SetIntialMainViewController("HomeVCID")

    }
    

}
