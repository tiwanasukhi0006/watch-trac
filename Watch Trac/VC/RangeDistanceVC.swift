//
//  RangeDistanceVC.swift
//  Watch Trac
//
//  Created by Sukhwinder Singh on 10/08/20.
//  Copyright © 2020 Sukhwinder Singh. All rights reserved.
//

import UIKit

class RangeDistanceVC: UIViewController {
    @IBOutlet weak var mupdateBtnOut: UIButton!
    
    @IBOutlet weak var mRangeTxt: UITextField!
    var rangeDis = Double()
    var categoryPicker = UIPickerView()

    override func viewDidLoad() {
        super.viewDidLoad()
        if let range = Constants.kUserDefaults.value(forKey: appConstants.rangeDistance)as? Double{
             rangeDis = range
            if range == 5.0{
                mRangeTxt.text = AppConstants.rangeArray[0]
            }else if range == 10.0{
                mRangeTxt.text = AppConstants.rangeArray[1]
            }else if range == 15.0{
                mRangeTxt.text = AppConstants.rangeArray[2]
            }else if range == 20.0{
                mRangeTxt.text = AppConstants.rangeArray[3]
            }else if range == 25.0{
                mRangeTxt.text = AppConstants.rangeArray[4]
            }
          }else{
            rangeDis = 0.0
          }
        mRangeTxt.delegate = self
        categoryPicker.delegate = self
        mRangeTxt.inputView = categoryPicker
        categoryPicker.backgroundColor = .white
        mRangeTxt.setLeftPaddingPoints(14)
    }
    override func viewWillAppear(_ animated: Bool) {
             super.viewWillAppear(animated)
             self.navigationController?.navigationBar.isHidden = false
             self.setNeedsStatusBarAppearanceUpdate()
             self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
             self.navigationController?.navigationBar.shadowImage = UIImage()
             self.navigationController?.navigationBar.isTranslucent = true
         }
         
         override func viewWillDisappear(_ animated: Bool) {
             super.viewWillDisappear(animated)
             self.navigationController?.navigationBar.setBackgroundImage(nil, for: .default)
             self.navigationController?.navigationBar.shadowImage = nil
             self.navigationController?.navigationBar.isTranslucent = true
         }
    @IBAction func mBackBtnAct(_ sender: Any) {
        kConstantObj.SetIntialMainViewController("HomeVCID")
    }
    
    @IBAction func mUpdateBtnAct(_ sender: Any) {
        if rangeDis == 0.0{
            NotificationAlert().NotificationAlert(titles: "Select Range")
            return
        }
        Constants.kUserDefaults.set(self.rangeDis, forKey: appConstants.rangeDistance)
        kConstantObj.SetIntialMainViewController("HomeVCID")

    }
    
}
extension RangeDistanceVC : UIPickerViewDataSource,UIPickerViewDelegate,UITextFieldDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return AppConstants.rangeArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return AppConstants.rangeArray[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.mRangeTxt.text = AppConstants.rangeArray[row]
        self.rangeDis = AppConstants.rangeArrayMaxNo[row]
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        Constants.kUserDefaults.set(self.rangeDis, forKey: appConstants.rangeDistance)
    }
  
    
    
}
